'use strict';

describe('BookingsCtrl', function () {
  var BookingsCtrl,
    scope;

  // load the controller's module
  beforeEach(module('taksi_client'));
  beforeEach(module('starter.controllers'));

  beforeEach(inject(function($rootScope, $controller) {
      scope = $rootScope.$new();
      $controller('BookingsCtrl', {$scope: scope});
  }));

  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BookingsCtrl = $controller('BookingsCtrl', {
      $scope: scope
    });
  }));

  var $httpBackend = {};

  beforeEach(inject(function (_$httpBackend_) {
    $httpBackend = _$httpBackend_;
  }));
  
  it('should submit a request to the backend service', function () {
    
    $httpBackend
    .expectPOST('http://localhost:3000/bookings',
          {latitude: 58.37, longitude: 26.71, destination: ""})
    .respond(201, {message: 'Booking is being processed'});
  });

});