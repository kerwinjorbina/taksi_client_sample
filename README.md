### What is this repository for? ###

* Quick summary
For Agile Software Development Course Project (Taxi Reservation System) Client

### How do I get set up? ###

* Summary of set up
Fork/Clone this Repository
* Configuration
* Dependencies
* Database configuration
* How to run tests
You should have at least 2 terminals to work on

1. pwd = taksi_client folder
- run the command: **node index.js**
- this will run the localhost for front-end application

2. pwd = taksi_client folder
- this is where you run the test for karma
- run the command : **karma start**


* Deployment instructions

Client Set-up

1. Go to taksi_client folder

2. run the command **npm install express --save**

3. run **npm install -D karma-jasmine karma-phantomjs-launcher --save**

4. run **npm install -g karma --save**

5. run **npm install -g karma@canary phantomjs karma-phantomjs-launcher**

6. run **npm install -g karma-jasmine**

7. run **npm install**

### Contribution guidelines ###

To contribute in this repository, please do the following instructions:

1. Fork this repository
2. Do not push to the master branch.
3. When pushing, create a pull request by:
git push <name_of_this_remote_in_your_pc> <name_of_your_local_branch>:integrate-<name_of_your_branch_in_remote>
4. Create a pull request.
- Please make sure that your changes don't introduce additional whitespaces
- What you are committing is what you have said in your commit message, nothing more and nothing less

### Who do I talk to? ###

* Repo owner or admin
Kerwin Jorbina (kerwin.jorbina@gmail.com)
* Other community or team contact
Anton Yeshchenko
Tiina Kuura
Fortunat Mutunda