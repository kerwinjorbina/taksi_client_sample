angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, LoginUser, LogoutUser, $rootScope, localStorageService, CurrentUser, $ionicPopup, $window) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  $scope.logout = function() {
    var confirmPopup = $ionicPopup.confirm({
     title: 'Confirm Logout',
     template: 'Are you sure you want to logout?'
    });
    confirmPopup.then(function(res) {
      if(res) {
        LogoutUser.save({ id: localStorageService.cookie.get('user_id') }, function(response) {
          localStorageService.cookie.clearAll();
          $window.location.href = '/';
        });
      }
    });
  };

  $scope.register = function() {
    $scope.closeLogin();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    LoginUser.save({ email: $scope.loginData.email, password: $scope.loginData.password}, function(response) {
      if(response.user_id){
        localStorageService.cookie.set('user_id', response.user_id);
        $window.location.href = '/';
      }
      else{
        var alertPopup = $ionicPopup.alert({
          title: 'User credentials are invalid',
        });
        alertPopup.then(function(res) {
          console.log('User credentials are invalid');
        });
      }
    });
    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 5000);
  };

  if (localStorageService.cookie.get('user_id') > 0){
    $scope.is_user = false;
    CurrentUser.get({ id: localStorageService.cookie.get('user_id') }, function(response) {
        $scope.user_name = response.name;
    });
  }
  else{
    $scope.is_user = true;
    $scope.user_name = "";
  }

});
