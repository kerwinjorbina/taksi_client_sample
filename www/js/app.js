// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('taksi_client', ['ionic', 'starter.controllers', 'ngRoute', 'ngResource', 'LocalStorageModule']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.book', {
    url: '/book',
    views: {
      'menuContent': {
        templateUrl: 'templates/book.html',
        controller: 'BookingsCtrl'
      }
    }
  })

  .state('app.findingtaxi', {
    url: '/find/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/findingtaxi.html',
        controller: 'FindingTaxiCtrl'
      }
    }
  })

  .state('app.bookdetails', {
    url: '/book/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/success.html',
        controller: 'BookingsDetailsCtrl'
      }
    }
  })

  .state('app.taxidetails', {
    url: '/taxi/:id',
    controller: 'TaxiDetailsCtrl'
  })

  .state('app.settings', {
    url: '/settings',
    views: {
      'menuContent': {
        templateUrl: 'templates/settings.html',
        controller: 'SettingsCtrl'
      }
    }
  })

  .state('app.register', {
    url: '/register',
    views: {
      'menuContent': {
        templateUrl: 'templates/register.html',
        controller: 'RegisterCtrl'
      }
    }
  })
  
  .state('app.bookingshistory', {
    url: '/bookingshistory',
    views: {
      'menuContent': {
        templateUrl: 'templates/bookingshistory.html',
        controller: 'AboutCtrl'
      }
    }
  })

  .state('app.about', {
    url: '/about',
    views: {
      'menuContent': {
        templateUrl: 'templates/about.html'
      }
    }
  })

  .state('app.balance', {
    url: '/balance',
    views: {
      'menuContent': {
        templateUrl: 'templates/balance.html',
         controller: 'BalanceCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/about');
});

app.factory('BackEnd', function() {
  return {
      link : 'http://immense-bastion-2867.herokuapp.com'
  };
});


app.config(function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('taksi_client')
    .setStorageType('sessionStorage')
    .setNotify(true, true)
});

app.factory('Framework', function ($q) {
  var _navigator = $q.defer();
  var _cordova = $q.defer();

  if (window.cordova === undefined) {
    _navigator.resolve(window.navigator);
    _cordova.resolve(false);
  } else {
    document.addEventListener('deviceready', function (evt) {
      _navigator.resolve(navigator);
      _cordova.resolve(true);
    });
  }

  return {
    navigator: function() { return _navigator.promise; },
    cordova: function() { return _cordova.promise; }
  };
});
