'use strict'
/* global angular */

var app = angular.module('taksi_client');

app.service('LoginUser', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/login', {});
});

app.service('LogoutUser', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/logout', {});
});

app.service('CurrentUser', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/user/:id', {});
});

app.service('About', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/userbookings', {});
});

app.service('SaveUser', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/user', {});
});

app.service('CurrentDriver', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/driver/:id', {});
});