'use strict'
/* global angular */

var app = angular.module('taksi_client');

app.service('BookingsService', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/bookings', {});
});

app.service('BookingsDetails', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/bookings/:id', {});
});

app.service('EndBooking', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/endbooking/:id', {});
});

app.service('CancelBooking', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/cancel/:id', {});
});