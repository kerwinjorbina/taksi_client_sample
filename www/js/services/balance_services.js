'use strict'
/* global angular */

var app = angular.module('taksi_client');

app.service('AddBalanceUser', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/add_balance_user', {});
});