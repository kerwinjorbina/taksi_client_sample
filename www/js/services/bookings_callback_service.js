'use strict';

var app = angular.module('taksi_client');

app.service('PusherService', function ($rootScope) {
  var pusher = new Pusher('0cc797f9d0193255595a');
  var channel = pusher.subscribe('bookings');
  return {
    onMessage: function (callback) {
      channel.bind('async_notification', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
    },
    onDriverArrived: function (callback) {
      channel.bind('driver_arrived_async', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
    },
    onDriverPickup: function (callback) {
      channel.bind('driver_pickup_async', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
    },
    onDriverDropoff: function (callback) {
      channel.bind('driver_dropoff_async', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
    }
  };
});