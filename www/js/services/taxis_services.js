'use strict'
/* global angular */

var app = angular.module('taksi_client');

app.service('TaxisDetails', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/taxi/:id', {});
});