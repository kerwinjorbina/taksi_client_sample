'use strict'
/* global angular */

var app = angular.module('taksi_client');

app.service('RegisterUser', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/register', {});
});