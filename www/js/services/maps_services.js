'use strict'
/* global angular */

var app = angular.module('taksi_client');

app.service('GetCoordinates', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/coordinates', {});
});

app.service('TravelDuration', function ($resource, BackEnd) {
  return $resource(BackEnd.link+'/travelduration', {});
});
