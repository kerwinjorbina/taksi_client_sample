'use strict'
/* global angular */

var app = angular.module('taksi_client');

app.controller('RegisterCtrl', function ($scope, RegisterUser, $ionicPopup) {
	$scope.doRegistration = function() {
		RegisterUser.save({name: $scope.name, email: $scope.email, password: $scope.password}, function (response) {
			if(response.is_success){
				var alertPopup = $ionicPopup.alert({
					title: 'User is created',
				});
				alertPopup.then(function(res) {
					console.log('success in user creation');
				});
			} else {
				var alertPopup = $ionicPopup.alert({
					title: 'Error in user creation',
				});
				alertPopup.then(function(res) {
					console.log('error in user creation: response.errors');
				});
			}
	    });
	};
});

