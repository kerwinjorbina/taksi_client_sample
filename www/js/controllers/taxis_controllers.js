'use strict'
/* global angular */

var app = angular.module('taksi_client');

app.controller('FindingTaxiCtrl', function ($scope, $ionicPopup, $location, $timeout, BookingsDetails, $stateParams, EndBooking, $ionicHistory, CancelBooking) {

  $scope.cancel = function() {
    CancelBooking.save({bookingId: $stateParams.id}, function (response) {
      var alertPopup = $ionicPopup.alert({
        title: response.message,
      });
      alertPopup.then(function(res) {
        console.log(response.message);
        $ionicHistory.goBack();
      });
    });
  }

  var counter = 0;
  (function tick() {
    if(counter > 30){
      EndBooking.save({bookingId: $stateParams.id}, function (response) {
    		var alertPopup = $ionicPopup.alert({
    			title: response.message,
    		});
    		alertPopup.then(function(res) {
    			console.log('No taxi available, should redirect back to bookings page');
          $ionicHistory.goBack();
    			// $location.path('/app/book');
    		});
  	  });
    }
    else{
      counter++;
      BookingsDetails.get({ id: $stateParams.id }, function(data) {
        $scope.origin = data.originAddress;
        $scope.destination = data.destinationAddress;
        if(data.taxiID){
          counter = 0;
  	      var alertPopup = $ionicPopup.alert({
  	        title: 'Taxi is found!',
  	      });
  	      alertPopup.then(function(res) {
  	        console.log('Taxi is found');
  	        $location.path('/app/book/'+$stateParams.id);
  	      });
        }
        else{
          $timeout(tick, 1000);
        }
      });
    }
  })();
});

