'use strict'
/* global angular */

var app = angular.module('taksi_client');

app.controller('BookingsCtrl', function ($scope, BookingsService, CurrentUser, PusherService, $location, Framework, localStorageService, $ionicPopup) {

  function alertPopup(message){
    var alertPopup = $ionicPopup.alert({
      title: message,
    });
    alertPopup.then(function(res) {
      console.log(message);
    });
  }
  $scope.syncNotification = '';
  $scope.asyncNotification = '';
  $scope.submit = function() {
    CurrentUser.get({ id: localStorageService.cookie.get('user_id') }, function(response) {
      if(response.balance > 0){
        var userId = (localStorageService.cookie.get('user_id'))?localStorageService.cookie.get('user_id'):1;
        BookingsService.save({latitude: $scope.latitude, longitude: $scope.longitude, destination: $scope.destination, user_id: userId}, function (response) {
          if(userId == response.userId){
            $scope.syncNotification = response.message;
            alertPopup(response.message);
          }
        });        
      }
      else{
        alertPopup("Please load some balance first before making a booking");
      }
    });
  };

  PusherService.onMessage(function(response) {
      var userId = (localStorageService.cookie.get('user_id'))?localStorageService.cookie.get('user_id'):1;
      if(userId == response.userId){
        $scope.asyncNotification = response.message;
        $location.path('/app/find/'+response.booking);
      }
  });

  Framework.navigator().then(function (navigator) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var latitude = position.coords.latitude;
      var longitude = position.coords.longitude;
      var mapProp = {
        center: {lat: latitude, lng: longitude},
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      $scope.map = new google.maps.Map(document.getElementById('map'), mapProp);
      $scope.marker = new google.maps.Marker(
        { map: $scope.map, position: mapProp.center});

      $scope.$apply(function (){
        $scope.latitude = latitude;
        $scope.longitude = longitude;
      });
    });
  });
});


app.controller('BookingsDetailsCtrl', function ($scope, $stateParams, localStorageService, BookingsDetails, TaxisDetails, $location, $ionicPopup, Framework, GetCoordinates, PusherService, $ionicHistory, TravelDuration, CurrentDriver) {

  function openPopup(response){
    if(response.userId == localStorageService.cookie.get('user_id')){
      var alertPopup = $ionicPopup.alert({
        title: response.message,
      });
      alertPopup.then(function(res) {
        console.log(response.message);
      });
    }
  }
  PusherService.onDriverArrived(function(response) {
    openPopup(response);
  });

  PusherService.onDriverPickup(function(response) {
    openPopup(response);
  });

  PusherService.onDriverDropoff(function(response) {
    openPopup(response);
    $location.path('/app/book');
  });

  BookingsDetails.get({ id: $stateParams.id }, function(data) {
    if(data.status > 3){
      $location.path('/app/book');
    }
    $scope.address = data.originAddress;
    $scope.destination = data.destinationAddress;
    $scope.finding_taxi = true;

    TravelDuration.get({ origin: data.originAddress, destination: data.destinationAddress }, function(response) {
      $scope.eta = response.duration;
      $scope.distance = response.distance;
    });

    if(data.taxiID){
      $scope.finding_taxi = false;
      TaxisDetails.get({ id: data.taxiID }, function(response) {
        $scope.driver_name = response.driverName;
        $scope.operator = response.operator;
        $scope.plate_number = response.plateNr;
        CurrentDriver.get({ id: response.driverId }, function(driverDetails) {
          $scope.driver_name = driverDetails.name;
          $scope.contact_number = driverDetails.phoneNr;
        });
      });
    }

    GetCoordinates.save({ address: data.originAddress }, function(originAddressData) {
      if(originAddressData.is_success){
        GetCoordinates.save({ address: data.destinationAddress }, function(destinationAddressData) {
          if(destinationAddressData.is_success){
            Framework.navigator().then(function (navigator) {
              var directionsDisplay;
              var directionsService = new google.maps.DirectionsService();
              var map;
              var originLatLng = new google.maps.LatLng(originAddressData.latitude, originAddressData.longitude);
              var destinationLatLng = new google.maps.LatLng(destinationAddressData.latitude, destinationAddressData.longitude);

              function initialize() {
                directionsDisplay = new google.maps.DirectionsRenderer();
                var mapOptions = {
                  zoom: 14,
                  center: originLatLng
                }
                map = new google.maps.Map(document.getElementById("map"), mapOptions);
                directionsDisplay.setMap(map);
                $scope.map = map;
              }

              function calcRoute() {
                var request = {
                    origin: originLatLng,
                    destination: destinationLatLng,
                    travelMode: google.maps.TravelMode["DRIVING"]
                };
                directionsService.route(request, function(response, status) {
                  if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                  }
                });
              }
              initialize();
              calcRoute();
            });
          }
          else{
            console.log("message = "+addressData.message);
          }
        });
      }
      else{
        console.log("message = "+addressData.message);
      }
    });
  });
});
