'use strict'
/* global angular */

var app = angular.module('taksi_client');

app.controller('BalanceCtrl', function ($scope, $ionicPopup, $location, $timeout, localStorageService, BookingsDetails, $stateParams, AddBalanceUser, CurrentUser) {

  function updateBalance(){
    CurrentUser.get({ id: localStorageService.cookie.get('user_id') }, function(response) {
      $scope.balance = (response.balance).toFixed(2);
    });
  }
  $scope.addBalance = function() {
    var userId = (localStorageService.cookie.get('user_id'))?localStorageService.cookie.get('user_id'):1;
    var value = $("#addBalanceValue").val();
    AddBalanceUser.save({userId: userId, value: value}, function (response) {
      var alertPopup = $ionicPopup.alert({
        title: response.message,
      });
      alertPopup.then(function(res) {
        console.log(response.message);
        updateBalance();
      });
    });
  }

  updateBalance();
});

