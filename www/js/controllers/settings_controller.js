'use strict'
/* global angular */

var app = angular.module('taksi_client');

app.controller('SettingsCtrl', function ($scope, $stateParams, localStorageService, CurrentUser, $ionicPopup, SaveUser) {
		$scope.hide_psw = "visible";
        $scope.show_psw = "none";
		CurrentUser.get({ id: localStorageService.cookie.get('user_id') }, function(response) {
            $scope.name = response.name;
            $scope.email = response.email;
            $scope.phone = response.phoneNr;
        });	
			
        $scope.psw = function() {
		   	$scope.hide_psw = "none";
            $scope.show_psw = "visible";
		};

		$scope.submit = function() {
            SaveUser.save({id: localStorageService.cookie.get('user_id'), name: $scope.name, email: $scope.email,  phoneNr: $scope.phone}, function (data) {
                var alertPopup = $ionicPopup.alert({
					title: data.message,
				});
				alertPopup.then(function(res) {
					console.log('Changed user details');
				});
			});   
		};
});