'use strict'
/* global angular */

var app = angular.module('taksi_client');
console.log("loaded aboutController");
app.controller('AboutCtrl', function ($scope, $timeout, localStorageService, About, $stateParams) {
    console.log("loaded aboutCTRL function");
    About.query({ userId: localStorageService.cookie.get('user_id') }, function(data) {
        console.log(localStorageService.cookie.get('taxi_id'));
        $scope.bookings = data;
        console.log("get data");
    });
});
